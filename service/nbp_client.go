package service

import (
	"NBPApi/model"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"
	"time"
)

type NbpClient interface {
	GetCurrencyRates() (model.Currency, error)
	GetResponseStatusCode() int
	IsValidJson() bool
	GetResponseTime() time.Duration
}

type nbpClient struct {
	status       int
	isJson       bool
	responseTime time.Duration
}

func NewNbpClient() NbpClient {
	return &nbpClient{}
}

func (c *nbpClient) GetCurrencyRates() (model.Currency, error) {
	b, err := c.call()
	if err != nil {
		return model.Currency{}, err
	}

	if !json.Valid(b) {
		return model.Currency{}, errors.New("invalid JSON")
	}

	cur := model.Currency{}
	if err := json.Unmarshal(b, &cur); err != nil {
		return model.Currency{}, err
	}
	return cur, nil
}

func (c *nbpClient) GetResponseStatusCode() int {
	return c.status
}

func (c *nbpClient) IsValidJson() bool {
	return c.isJson
}

func (c *nbpClient) GetResponseTime() time.Duration {
	return c.responseTime
}

func (c *nbpClient) call() ([]byte, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", "https://api.nbp.pl/api/exchangerates/rates/a/eur/last/100/?format=json", nil)
	req.Header.Set("Host", "api.nbp.pl")
	req.Header.Set("User-Agent", "Insomnia/2023.5.7")

	startReq := time.Now().UnixMilli()
	r, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	c.responseTime = time.Duration(time.Now().UnixMilli() - startReq)
	c.status = r.StatusCode
	c.verifyContentType(r)

	b, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (c *nbpClient) verifyContentType(r *http.Response) {
	for k, v := range r.Header {
		if k == "Content-Type" {
			c.isJson = strings.Contains(v[0], "application/json")
			break
		}
	}
}
