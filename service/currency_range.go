package service

import (
	"NBPApi/model"
)

type CurrencyRange interface {
	GetOutOfRange(c model.Currency) []string
}

type currencyRange struct {
	min float64
	max float64
}

func NewCurrencyRange(min, max float64) CurrencyRange {
	return &currencyRange{
		min: min,
		max: max,
	}
}

func (cr currencyRange) GetOutOfRange(c model.Currency) []string {
	if len(c.Rates) == 0 {
		return nil
	}

	var outOfRange []string
	for _, v := range c.Rates {
		if cr.min > v.Mid || cr.max < v.Mid {
			outOfRange = append(outOfRange, v.EffectiveDate)
		}
	}
	return outOfRange
}
