package model

type Currency struct {
	Code     string
	Currency string
	Rates    []Rate
	Table    string
}
