package model

type Rate struct {
	EffectiveDate string
	Mid           float64
	No            string
}
