package main

import (
	"NBPApi/service"
	"fmt"
	"log"
	"os"
	"sync"
	"time"
)

func main() {
	fileName := fmt.Sprintf("%s.log", time.Now().Format("2006-01-02"))
	logFile, err := os.OpenFile(fileName, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panic(err)
	}
	defer logFile.Close()

	log.SetOutput(logFile)
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			client := service.NewNbpClient()
			c, err := client.GetCurrencyRates()
			if err != nil {
				fmt.Println(err)
				log.Println(err)
				return
			}

			fmt.Printf(
				"%s HTTP: %d, Response time: %s, is valid Json: %t\n",
				time.Now().Format("2006/01/02 15:04:05"),
				client.GetResponseStatusCode(),
				client.GetResponseTime(),
				client.IsValidJson())

			log.Printf(
				"HTTP: %d, Response time: %s, is valid Json: %t\n",
				client.GetResponseStatusCode(),
				client.GetResponseTime(),
				client.IsValidJson())

			cr := service.NewCurrencyRange(4.5, 4.7)
			outOfRange := cr.GetOutOfRange(c)
			if outOfRange != nil {
				fmt.Printf(
					"%s the currency was out of range in days: %v\n",
					time.Now().Format("2006/01/02 15:04:05"),
					outOfRange)
				log.Printf("the currency was out of range in days: %v\n", outOfRange)
			}
		}()
		time.Sleep(5 * time.Second)
	}
	wg.Wait()
}
